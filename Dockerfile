# -------------------------------------------------------
# 'build' stage
# -------------------------------------------------------

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env


# set working directory
WORKDIR /SonarConsoleApp


## Coppying code 
COPY . ./


### # run build command
RUN dotnet build
